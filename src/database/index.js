'use strict';

const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/database')[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

// add modules manually to prevent errors when use webpack
const modules = {
  User: require('./models/user'),
  Team: require('./models/team'),
  UsersTeams: require('./models/usersTeams'),
  Challenge: require('./models/challenge'),
  Sport: require('./models/sport')
};

Object.keys(modules).forEach((module) => {
  const model = modules[module](sequelize, Sequelize.DataTypes);
  db[module] = model;
});

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

// TODO: alter attribute not recommended for production use.
sequelize.sync({ alter: true });
db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
