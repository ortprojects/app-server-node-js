('use strict');
module.exports = (sequelize, DataTypes) => {
  const Team = sequelize.define(
    'Team',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      photoUrl: DataTypes.STRING,
      geoPoint: {
        type: DataTypes.GEOMETRY('POINT', 4326),
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    },
    {
      timestamps: true,
      underscored: true,
      tableName: 'Teams',
      defaultScope: {
        attributes: { exclude: ['sportId'] }
      },
      hooks: {
        beforeSave: function (instance) {
          if (instance.geoPoint && !instance.geoPoint.crs) {
            instance.geoPoint.crs = {
              type: 'name',
              properties: {
                name: 'EPSG:4326'
              }
            };
          }
        }
      }
    }
  );

  Team.associate = function (models) {
    models.Team.belongsToMany(models.User, {
      through: models.UsersTeams,
      as: 'users',
      onDelete: 'CASCADE'
    });

    models.Team.belongsTo(models.Sport, {
      as: 'sport',
      foreignKey: 'sportId',
      onDelete: 'CASCADE'
    });

    models.Team.belongsToMany(models.Team, {
      through: { model: models.Challenge, unique: false },
      as: 'challengers',
      foreignKey: {
        name: 'challengedToTeamId'
      },
      onDelete: 'CASCADE'
    });

    models.Team.belongsToMany(models.Team, {
      through: { model: models.Challenge, unique: false },
      as: 'challenged',
      foreignKey: {
        name: 'teamId'
      },
      onDelete: 'CASCADE'
    });
  };

  return Team;
};
