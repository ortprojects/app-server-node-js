'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sport = sequelize.define(
    'Sport',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    },
    {
      timestamps: true,
      underscored: true,
      tableName: 'Sports'
    }
  );

  Sport.associate = function (models) {
    //   models.Sport.belongsToMany(models.Team, {
    //     through: models.Team,
    //     foreignKey: 'sportId',
    //     targetKey: 'id'
    //   });
    // };
  };
  return Sport;
};
