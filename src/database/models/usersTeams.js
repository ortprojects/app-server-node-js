'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsersTeams = sequelize.define('UsersTeams', {
    userId: {
      type: DataTypes.STRING,
      primaryKey: true,
      references: {
        model: 'User',
        key: 'id'
      }
    },
    teamId: {
      type: DataTypes.STRING,
      primaryKey: true,
      references: {
        model: 'Team',
        key: 'id'
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    timestamps: true,
    underscored: true,
    tableName: 'Users_Teams'
  });
  return UsersTeams;
};
