'use strict';
module.exports = (sequelize, DataTypes) => {
  const Challenge = sequelize.define(
    'Challenge',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      teamId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        references: {
          model: 'Team',
          key: 'id'
        }
      },
      challengedToTeamId: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        references: {
          model: 'Team',
          key: 'id'
        }
      },
      tentativeDate: {
        type: DataTypes.DATE,
        isDate: true,
        allowNull: false
      },
      isAccepted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      }
    },
    {
      timestamps: true,
      underscored: true,
      tableName: 'Challenges',
      indexes: [
        {
          unique: true,
          fields: ['id', 'team_id', 'challenged_to_team_id']
        }
      ]
    }
  );

  Challenge.associate = function (models) {};

  return Challenge;
};
