'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      photoUrl: DataTypes.STRING,
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'password'
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'username'
      },
      isFirstTime: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      // Needed postgis extension in your system (Postgres)
      geoPoint: {
        type: DataTypes.GEOMETRY('POINT'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    },
    {
      timestamps: true,
      underscored: true,
      tableName: 'Users',
      defaultScope: {
        attributes: { exclude: ['UsersTeams', 'password'] }
      }
    }
  );
  User.associate = function(models) {
    // associations can be defined here
    models.User.belongsToMany(models.Team, {
      through: models.UsersTeams,
      as: 'teams'
    });
  };

  return User;
};
