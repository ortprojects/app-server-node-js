'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    var migrations = [];
    migrations.push(queryInterface.addColumn(
      'Users',
      'username',
      {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    ));
    return Promise.all(migrations);
  },
  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    var migrationDown = [];
    migrationDown.push(queryInterface.removeColumn('Users', 'username'));
    return Promise.all(migrationDown);
  }
};
