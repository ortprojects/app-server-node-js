import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config/constants';

const middleware = {
  async isAuthenticated(req, res, next) {
    if (!req.headers.authorization) {
      return res.status(403).send({
        message: 'No token provided!'
      });
    }
    const isBearer = req.headers.authorization.split(' ')[0] === 'Bearer';
    const token = req.headers.authorization.split(' ')[1];

    if (!isBearer && !token) {
      return res.status(403).send({
        message: 'No token provided!'
      });
    }

    jwt.verify(token, JWT_SECRET, (error, decoded) => {
      if (error) {
        return res.status(401).send({
          message: 'Unauthorized!'
        });
      }
      req.decoded = decoded;
      req.token = token;
      next();
    });
  }
};

export default middleware;
