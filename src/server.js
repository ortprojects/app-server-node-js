import 'core-js/stable';
import 'regenerator-runtime/runtime';
import app from './app.js';
import { PORT } from './config/constants.js';
import db from './database/index.js';

async function main() {
  try {
    await db.sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
  await app.listen(PORT);
  console.log(`Listening on port ${PORT}`);
}

main();
