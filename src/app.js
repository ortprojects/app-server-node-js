import 'core-js/stable';
import 'regenerator-runtime/runtime';
import express from 'express';
import restRouter from './modules/index.js';
import cors from 'cors';
import { API_PATH_BASE } from './config/constants.js';
import bodyParser from 'body-parser';
import GoogleCloudStorage from './modules/common/models/GoogleCloudStorage.js';
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(API_PATH_BASE, restRouter);

GoogleCloudStorage.initializeStorage();

app.use((req, res, next) => {
  const error = new Error('Not found');
  error.message = 'Invalid route';
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.json({
    error: {
      message: error.message
    }
  });
});

export default app;
