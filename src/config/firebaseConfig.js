import firebase from 'firebase-admin';
import dotenv from 'dotenv';
dotenv.config();
var serviceAccount = require('../../desaf-bcb48-firebase-adminsdk-zdyer-0512a05e88.json');

export default {
  initializeApp() {
    firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount),
      databaseURL: process.env.DB_FIREBASE_URL
    });
  }
};
