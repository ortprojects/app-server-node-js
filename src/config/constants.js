import dotenv from 'dotenv';
dotenv.config();

export const PORT = process.env.PORT || 4000;
export const API_PATH_BASE = `/${process.env.API_PATH}/${process.env.API_VERSION}`;
export const JWT_SECRET = process.env.JWT_SECRET;
