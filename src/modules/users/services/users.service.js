import { User } from '../index.js';
import { Team } from '../../teams/index.js';
import { Sport } from '../../sports';
import { Op } from 'sequelize';
import Encrypt from '../../auth/models/Encrypt.js';
/**
 * User service handle all interactions with database
 */

const userService = {
  /**
   * TODO: // implements all functions using promises
   */

  /**
   * Find user by primary key
   * @param {string} userId
   * @returns {Promise<User[]>} Promise<User[]>
   */
  findUserByPK(userId) {
    const user = User.findByPk(userId, {
      include: [
        {
          model: Team,
          as: 'teams'
        }
      ]
    });

    return user;
  },

  /**
   * Fetch users
   * @param {string} username
   * @returns {Promise<User[]>} Promise<User[]>
   */
  fetchUsers(username = '') {
    username = username.toLowerCase();

    const users = User.findAll({
      where: {
        username: {
          [Op.substring]: username
        }
      },
      include: [
        {
          model: Team,
          as: 'teams'
        }
      ],
      attributes: { exclude: ['password'] }
    });
    return users;
  },

  /**
   * Create and save user in database
   * @param {User} user - User to save
   * @returns {Promise}
   */
  async createUser(user) {
    user.password = await Encrypt.genHash(user.password);
    const newUser = User.create(user);
    return newUser;
  },

  /**
   * Search user by fields
   * @param {Map<string, string>} fields - Example: {name: 'New Name'}
   * @returns {Promise<User>}
   */
  searchUserByFields(fields) {
    const user = User.findOne({
      where: fields,
      include: [
        {
          model: Team,
          as: 'teams'
        },
        {
          model: Sport,
          as: 'sport'
        }
      ]
    });

    return user;
  },

  /**
   * Update geolocation
   * @param {Map<string, any>} values - 'Values to updated, example: {name: 'New Name'}
   * @param {number} userId - Id to update
   * @returns {Promise<User>}
   */
  updateUserByPK(values, userId) {
    const options = {
      where: {
        id: userId
      }
    };

    const updatedUser = User.update(values, options);

    return updatedUser;
  },

  /**
   * Search user by fields with password
   * @param {Map<string, string>} fields - Example: {name: 'New Name'}
   * @returns {Promise<User>}
   */
  searchUserByFieldsWithPassword(fields) {
    const user = User.findOne({
      where: fields,
      include: [
        {
          model: Team,
          as: 'teams'
        }
      ],
      attributes: { include: ['password'] }
    });

    return user;
  },

  /**
   * Search user by name
   * @param {string} name - User to save
   * @returns {Promise}
   */
  searchUserByName(name) {
    const users = User.findAll({
      where: {
        name: {
          [Op.substring]: name
        }
      }
    });

    return users;
  }
};

export default userService;
