import express from 'express';
import usersRoutes from './routes/users.routes';
import multer from '../common/models/MulterModel';
/**
 * Initialize and create routes
 */
const userRouter = express.Router();

/**
 * TODO: implements all routes
 */
userRouter.route('/').get(usersRoutes.fetchUsers);
userRouter.route('/').post(usersRoutes.createUser);
userRouter.route('/:userId').post(usersRoutes.updateUserBYyPK);
userRouter.route('/:userId/photoUrl').post(multer.single('photoUrl'), usersRoutes.updateUserPhotoUrlByPK);

export default userRouter;
