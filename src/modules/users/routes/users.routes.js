import userController from '../controllers/users.controller';
import GoogleCloudStorage from '../../common/models/GoogleCloudStorage';
/**
 * Users routes handle all HTTP calls only
 */

export default {
  /**
   * @param {*} req - Request from express
   * @param {*} res - Response from express
   */
  async fetchUsers(req, res) {
    const { username } = req.query;
    try {
      const users = await userController.fetchusers(username);

      return res.status(200).send({
        message: '',
        data: users
      });
    } catch (error) {
      console.log(error);

      return res.status(500).send({
        message: '',
        data: []
      });
    }
  },

  /**
   * @param {*} req - Request from express
   * @param {*} res - Response from express
   */

  createUser(req, res) {
    const { user } = req.body;
    try {
      const newUser = userController.createUser(user);

      return res.status(200).send({
        message: '',
        data: newUser
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      });
    }
  },

  searchUserByName(req, res) {
    try {
      const { name } = req.query;

      const user = userController.searchUserByName(name);

      return res.status(200).send({
        message: '',
        data: user
      });
    } catch (error) {
      return res.status(500).send({
        message: error.message,
        data: []
      });
    }
  },

  /**
   * @param {*} req - Request from express
   * @param {*} res - Response from express
   */
  async updateUserBYyPK(req, res) {
    try {
      const { user } = req.body;

      const userUpdated = await userController.updateUserByPK(user);

      return res.status(200).send({
        message: 'User updated successfully'
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: error.message
      });
    }
  },

  async updateUserPhotoUrlByPK(req, res) {
    try {
      const { user } = req.body;
      const file = req.file;
      const userParse = JSON.parse(user);
      const urlImage = await GoogleCloudStorage.uploadImageToStorage(file);
      userParse.photoUrl = urlImage;

      const userUpdated = await userController.updateUserByPK(userParse);

      return res.status(200).send({
        message: 'User updated successfully'
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: error.message
      });
    }
  }
};
