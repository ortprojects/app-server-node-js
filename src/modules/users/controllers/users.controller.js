import userService from '../services/users.service';
import usersService from '../services/users.service';

/**
 * Users controller call services functions
 */

export default {
  /**
   * Fetch users
   * @param {string} username
   * @returns {Promise<User[]>}
   */
  fetchusers(username) {
    return userService.fetchUsers(username);
  },

  /**
   * Create and save user in database
   * @param {User} user
   */
  createUser(user) {
    return userService.addUser(user);
  },

  /**
   * Search user by name
   * @param {string} name
   */
  searchUserByName(name) {
    return usersService.createUser(name);
  },

  /**
   * Update geolocation
   * @param {User} user - user values to update
   * @returns {Promise<User>}
   */
  updateUserByPK(user) {
    let values = {};
    if (user.latitude && user.longitude) {
      const geoPoint = {
        type: 'Point',
        coordinates: [user.latitude, user.longitude]
      };

      user.geoPoint = geoPoint;
      values = { ...values, geoPoint };
      delete user.latitude;
      delete user.longitude;
    }
    values = user;

    return usersService.updateUserByPK(values, user.id);
  }
};
