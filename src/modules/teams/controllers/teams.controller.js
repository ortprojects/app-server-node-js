import teamService from '../services/teams.service';

/**
 * Controller call services functions
 */

export default {
  /**
   * Fetch teams
   * @param {{}} options
   * @returns {Promise<User[]>}
   */
  fetchTeams(options) {
    return teamService.fetchTeams(options);
  },

  /**
   * Create and save team in database
   * @param {Team} team
   */
  createTeam(team) {
    const geoPoint = {
      type: 'Point',
      coordinates: [team.longitude, team.latitude],
      crs: { type: 'name', properties: { name: 'EPSG:4326' } }
    };

    team.geoPoint = geoPoint;
    return teamService.createTeam(team);
  },

  /**
   * Fetch teams challenges
   * @param {number[]} teamIds
   */
  fetchTeamsChallenges(teamIds) {
    return teamService.fetchTeamsChallenges(teamIds);
  },

  /**
   * Fetch teams challenges
   * @param {number} teamId
   */
  fetchTeamChallengesByPK(teamId) {
    return teamService.fetchTeamChallengesByPK(teamId);
  }
};
