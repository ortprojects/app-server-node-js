import { Team } from '../index';
import { User } from '../../users/index';
import { userTeamService } from '../../usersTeams/index';
import db from '../../../database/index';
import { Sport } from '../../sports';
/**
 * Service handle all interactions with database
 */

export default {
  /**
   * TODO: // implements all functions using promises
   */

  /**
   * Fetch teams
   * @param {{}} options
   * @returns {Promise<Team[]>} Promise<Team[]>
   */
  async fetchTeams(options) {
    const latitude = options.lat;
    const longitude = options.long;
    const Sequelize = db.Sequelize;

    const teams = Team.findAll({
      attributes: {
        include: [
          [
            Sequelize.fn(
              'ST_Distance',
              Sequelize.col('Team.geo_point'),
              Sequelize.fn(
                'St_SetSRID',
                Sequelize.fn('ST_MakePoint', longitude, latitude),
                4326
              )
            ),
            'distance'
          ]
        ]
      },
      include: [
        {
          model: User,
          as: 'users'
        },
        {
          model: Sport,
          as: 'sport'
        }
      ],
      where: Sequelize.where(
        Sequelize.fn(
          'ST_DWithin',
          Sequelize.col('Team.geo_point'),
          Sequelize.literal(
            `ST_MakePoint(${longitude},${latitude})::geography`
          ),
          3000 // meters
        ),
        true
      ),
      order: Sequelize.literal('distance ASC')
    });

    return teams;
  },

  /**
   * Fetch teams challenges
   * @param {number[]} teamIds
   * @returns {Promise<Team[]>} Promise<Team[]>
   */
  fetchTeamsChallenges(teamIds) {
    const teams = Team.findAll({
      where: {
        id: teamIds
      },
      include: [
        {
          model: Team,
          as: 'challenged'
        },
        {
          model: Team,
          as: 'challengers'
        },
        {
          model: Sport,
          as: 'sport'
        },
        {
          model: User,
          as: 'users',
          through: {
            attributes: []
          }
        }
      ]
    });

    return teams;
  },

  /**
   * Fetch teams challenges
   * @param {number} teamId
   * @returns {Promise<Team>} Promise<Team[]>
   */
  fetchTeamChallengesByPK(teamId) {
    const team = Team.findByPk(teamId, {
      include: [
        {
          model: Team,
          as: 'challenged'
        },
        {
          model: Team,
          as: 'challengers'
        },
        {
          model: Sport,
          as: 'sport'
        },
        {
          model: User,
          as: 'users',
          through: {
            attributes: []
          }
        }
      ]
    });

    return team;
  },

  /**
   * Create and save team in database
   * @param {Team} team - Team to save
   * @param {User[]} users - Team to save
   * @returns {Promise<void>}
   */
  async createTeam(team) {
    const transaction = await db.sequelize.transaction(async (t) => {
      const newTeam = await Team.create(team, { transaction: t });

      var promises = [];

      team.users.forEach((user) => {
        const userTeams = userTeamService.create(user.id, newTeam.id, {
          transaction: t
        });
        promises.push(userTeams);
      });

      return Promise.all(promises);
    });

    return transaction;
  }
};
