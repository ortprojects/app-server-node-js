import teamRouter from './routes';
import Team from './models/Team';
export {
  Team,
  teamRouter
};
