import mocha from 'mocha';
import app from '../../../../app';
import request from 'supertest';
import dotenv from 'dotenv';
import chai from 'chai';
import chaiHttp from 'chai-http';
import { API_PATH_BASE } from '../../../../config/constants';

dotenv.config();
const assert = chai.assert;
const describe = mocha.describe;
const expect = chai.expect;
const it = mocha.it;
const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTg5OTE1NTM5LCJleHAiOjE1OTAwMDE5Mzl9.B2uXi0pZFrUVZgUvavH08KTeJl1VkF18PXYob8rHyzA';

chai.use(chaiHttp);
describe('Team routes', function () {
  describe('GET -> Get teams', function () {
    it('should return teams by location', function (done) {
      const lat = -34.618908999999995;
      const long = -58.4560331;
      chai
        .request(app)
        .get(`${API_PATH_BASE}/teams?lat=${lat}&long=${long}`)
        .set({ Authorization: `Bearer ${token}` })
        .end(function (err, res) {
          expect(res.status).to.equal(200)
          expect(res.body.data).to.be.an('array');
          done(); // <= Call done to signal callback end
        });
    });
  });

  describe('GET -> Get teams without location', function () {
    it('should return error 404', function (done) {
      request(app)
        .get(`/api/v1/teams`)
        .set({ Authorization: `Bearer ${token}` })
        .end(function (err, res) {
          expect(res.status).to.equal(404)
          expect(res.body.message).to.equal(
            'Location is necessary for search teams, please enable.'
          );
          done();
        });
    });
  });
});
