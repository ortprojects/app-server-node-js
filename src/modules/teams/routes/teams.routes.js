import teamController from '../controllers/teams.controller';
import GoogleCloudStorage from '../../common/models/GoogleCloudStorage';
import isEmpty from 'validator/lib/isEmpty';
import util from 'util';

/**
 * Routes handle all HTTP calls only
 */

export default {
  /**
   * @param {*} req - Request from express
   * @param {*} res - Response from express
   */
  async fetchTeams(req, res) {
    const { lat, long } = req.query;
    
    if (!lat || !long) {
      return res.status(404).send({
        message: 'Location is necessary for search teams, please enable.'
      });
    }

    try {
      const teams = await teamController.fetchTeams({ lat, long });
      return res.status(200).send({
        message: '',
        data: teams
      });
    } catch (error) {
      console.log(error);

      return res.status(500).send({
        message: error.message
      });
    }
  },

  /**
   * @param {*} req - Request from express
   * @param {*} res - Response from express
   */

  async addTeam(req, res, next) {
    try {
      const { team } = req.body;

      const teamParse = JSON.parse(team);
      if (isEmpty(teamParse.name)) {
        throw new Error('Please enter a name to Team');
      }
      if (teamParse.users.length === 0) {
        throw new Error('Please add at least one User');
      }
      if (!teamParse.latitude) {
        throw new Error('Please enter a location');
      }
      if (!teamParse.longitude) {
        throw new Error('Please enter a location');
      }

      const file = req.file;

      const urlImage = await GoogleCloudStorage.uploadImageToStorage(file);

      teamParse.photoUrl = urlImage;
      await teamController.createTeam(teamParse);

      return res.status(200).send({
        message: 'Team was created successfully',
        data: []
      });
    } catch (error) {
      console.log(error);

      return res.status(500).send({
        message: error.message
      });
    }
  },

  async fetchTeamsChallenges(req, res) {
    try {
      const { teamIds } = req.body;
      const teams = await teamController.fetchTeamsChallenges(teamIds);

      return res.status(200).send({
        message: '',
        data: teams
      });
    } catch (error) {
      console.log(error);

      return res.status(500).send({
        message: error.message,
        data: []
      });
    }
  },

  async fetchTeamChallengesByPK(req, res) {
    try {
      const { teamId } = req.params;
      const team = await teamController.fetchTeamChallengesByPK(teamId);
      return res.status(200).send({
        message: '',
        data: team
      });
    } catch (error) {
      console.log(error);

      return res.status(500).send({
        message: error.message,
        data: []
      });
    }
  }
};
