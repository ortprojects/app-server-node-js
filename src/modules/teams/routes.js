import express from 'express';
import teamRoutes from './routes/teams.routes';
import multer from '../common/models/MulterModel';
/**
 * Initialize and create routes
 */
const teamRouter = express.Router();

/**
 * TODO: implements all routes
 */
teamRouter.route('/?').get(teamRoutes.fetchTeams);
teamRouter.route('/').post(multer.single('image'), teamRoutes.addTeam);
teamRouter.route('/challenges').post(teamRoutes.fetchTeamsChallenges);
teamRouter.route('/:teamId/challenges').get(teamRoutes.fetchTeamChallengesByPK);

export default teamRouter;
