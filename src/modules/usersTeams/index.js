import UsersTeams from './models/usersTeams';
import userTeamService from './services/userTeams.service';

export {
  UsersTeams,
  userTeamService
};
