import { UsersTeams } from '../index';

const userTeamsService = {
  create(userId, teamId, options) {
    const userTeam = UsersTeams.create({
      teamId: teamId,
      userId: userId
    }, options);

    return userTeam;
  }
};

export default userTeamsService;
