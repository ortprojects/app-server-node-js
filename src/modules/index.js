import express from 'express';
import { teamRouter } from './teams';
import { userRouter } from './users';
import { authRouter } from './auth';
import middleware from '../middlewares/middlewares';

const restRouter = express.Router();
restRouter.use('/auth', authRouter);
restRouter.use('/users', middleware.isAuthenticated, userRouter);
restRouter.use('/teams', middleware.isAuthenticated, teamRouter);

export default restRouter;
