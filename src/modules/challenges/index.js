import challengeRouter from './routes';
import Challenge from './models/Challenge';
import challengeService from './services/challenges.service';
export {
  challengeRouter,
  Challenge,
  challengeService
};
