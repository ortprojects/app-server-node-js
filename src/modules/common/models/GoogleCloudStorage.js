import { Storage } from '@google-cloud/storage';
import { format } from 'util';

const bucketName = 'bucket-name';

export default class GoogleCloudStorage {
  static initializeStorage() {
    this.googleStorage = new Storage({
      keyFilename: 'desaf-bce7a1405deb.json',
      projectId: 'desaf-bcb48'
    });
    this.bucket = this.googleStorage.bucket('desaf-bcb48.appspot.com');
  }

  static async createBucket() {
    // Creates the new bucket
    await this.googleStorage.createBucket(bucketName);
    console.log(`Bucket ${bucketName} created.`);
  }

  static getPublicUrl(filename) {
    return `https://storage.googleapis.com/${this.bucket}/${filename}`;
  }

  /**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */
  static uploadImageToStorage(file) {
    return new Promise((resolve, reject) => {
      if (!file) {
        reject(new Error('No image file'));
      }
      const newFileName = `${file.originalname}_${Date.now()}`;
      const fileUpload = this.bucket.file(newFileName);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype
        }
      });

      blobStream.on('error', (error) => {
        reject(error);
      });

      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        const url = format(
          `https://storage.googleapis.com/${this.bucket.name}/${fileUpload.name}`
        );
        resolve(url);
      });

      blobStream.end(file.buffer);
    });
  }
}
