import { userService } from '../../users';
import Encrypt from '../models/Encrypt';
import authService from '../services/auth.service';
import isEmail from 'validator/lib/isEmail';
import isEmpty from 'validator/lib/isEmpty';

const authRoutes = {
  async signUp(req, res) {
    try {
      const { user } = req.body;

      if (isEmpty(user.email)) throw new Error('Please enter email');
      if (isEmpty(user.password)) throw new Error('Please enter password');
      if (isEmpty(user.username)) throw new Error('Please enter username');
      if (!isEmail(user.email)) throw new Error('Email is invalid');
      const newUser = await userService.createUser(user);
      const userRet = newUser.toJSON();
      delete userRet.password;

      return res.status(201).send({
        message: 'User saved successfully',
        data: userRet
      });
    } catch (error) {
      console.log(error); 

      return res.status(500).send({
        message: error.message
      });
    }
  },

  async signIn(req, res) {
    try {
      const { email, password } = req.body;

      if (isEmpty(email)) throw new Error('Please enter email');
      if (isEmpty(password)) throw new Error('Please enter password');
      if (!isEmail(email)) throw new Error('Email is invalid');

      const user = await userService.searchUserByFieldsWithPassword({ email });

      if (!user) throw new Error('There is no user with those credentials');

      const isValidPassword = await Encrypt.compare(password, user.password);

      if (!isValidPassword) {
        return res.status(401).send({
          accessToken: null,
          message: 'Invalid password'
        });
      }

      const payload = {
        id: user.id
      };
      const accessToken = authService.generateToken(payload);
      const userRet = user.toJSON();
      delete userRet.password;

      res.status(200).send({
        message: 'Sign in successfully',
        data: userRet,
        accessToken
      });
    } catch (error) {
      console.log(error);

      res.status(500).send({
        message: error.message
      });
    }
  },

  async checkToken(req, res) {
    try {
      const { id } = req.decoded;
      const accessToken = req.token;
      const user = await userService.findUserByPK(id);

      return res.status(200).send({
        data: user,
        accessToken
      });
    } catch (error) {
      res.status(500).send({
        message: error.message
      });
    }
  }
};

export default authRoutes;
