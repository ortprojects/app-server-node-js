import express from 'express';
import authRoutes from './routes/auth.routes';
import middleware from '../../middlewares/middlewares';

const authRouter = express.Router();

authRouter.route('/signin').post(authRoutes.signIn);
authRouter.route('/signup').post(authRoutes.signUp);
authRouter.route('/token').get(middleware.isAuthenticated, authRoutes.checkToken);

export default authRouter;
