import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../../../config/constants';

const authService = {
  generateToken(payload) {
    const token = jwt.sign(payload, JWT_SECRET, {
      expiresIn: 86400 // 24hs
    });

    return token;
  }
};

export default authService;
