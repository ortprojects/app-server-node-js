import bcrypt from 'bcryptjs';

export default {
  genSalt() {
    return bcrypt.genSaltSync(12);
  },
  genHash(rawData) {
    const salt = this.genSalt();
    return bcrypt.hash(rawData, salt);
  },
  compare(rawData, hash) {
    return bcrypt.compare(rawData, hash);
  }
};
