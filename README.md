## Install Dependencies

```shell
npm install
```

## Run application dev mode

```shell
npm start
```

## Extras

This project use conventional commits, you can show documentation here:
* [Commit-Message-Guidelines](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) - Documentation official
