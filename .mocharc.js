'use strict';
module.exports = {
  diff: true,
  extension: ['js'],
  opts: './src/modules/**/*.spec.js',
  package: './package.json',
  reporter: 'spec',
  slow: 75,
  recursive: true,
  timeout: 2000,
  ui: 'bdd'
};
